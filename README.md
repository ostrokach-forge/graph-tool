# graph-tool conda recipe

## Note

`graph-tool` is now [available on conda-forge](https://github.com/conda-forge/graph-tool-feedstock) and can be installed using `conda install -c conda-forge graph-tool`. 

*The package build by this repo (`graph-tool` from the `ostrokach-forge` channel) is no longer supported!*

## Installation

Install graph-tool (into a new conda environment to be safe):

```bash
conda create -n graph-tool -c conda-forge/label/gcc -c defaults -c ostrokach-forge --override-channels 'graph-tool>=2.28.dev0'
```

Test your graph-tool installation:

```bash
source activate graph-tool
python -c "from graph_tool.all import *"
```

## FAQ

**Why not submit this recipe to [conda-forge](https://conda-forge.github.io/)?**

<strike>Building graph-tool requires > 4 GB memory, which is more than what is provided by TravisCI and CircleCI.</strike>

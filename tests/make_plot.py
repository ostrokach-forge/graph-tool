import os
import os.path as op

import graph_tool.all as gt


def make_plot():
    g = gt.price_network(3000)
    pos = gt.sfdp_layout(g)
    gt.graph_draw(g, pos=pos, output=op.join(os.getcwd(), "graph-draw-sfdp.png"))


if __name__ == '__main__':
    make_plot()

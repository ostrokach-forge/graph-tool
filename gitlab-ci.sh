#!/bin/bash

if [[ -z $1 ]] ; then
  echo "Required environment variables have not been set!"
  exit -1
fi

mkdir -p conda-bld

gitlab-runner exec docker \
    --env CI_PROJECT_NAME="$(basename $(pwd))" \
    --env CI_PROJECT_NAMESPACE="$(basename $(dirname $(pwd)))" \
    --env HOST_USER_ID="$(id -u)" \
    --env HOST_GROUP_ID="$(id -g)" \
    --docker-volumes `pwd`/conda-bld:/opt/conda/conda-bld \
    $1

#!/bin/bash

set -ev

export PKG_INSTALLDIR="${PREFIX}"

# export CXXFLAGS=$(echo "${CXXFLAGS}" | sed 's/std=c++17/std=c++14/g')
# export DEBUG_CXXFLAGS=$(echo "${DEBUG_CXXFLAGS}" | sed 's/std=c++17/std=c++14/g')

# Dependencies have corrupt libtool files, leading to error:
# `../lib/libiconv.la' is not a valid libtool archive`
rm -f $PREFIX/lib/*.la

./autogen.sh
./configure \
    --enable-openmp \
    --enable-static \
    --prefix="$PREFIX" \
    --with-boost="$PREFIX" \
    --with-boost-libdir="$PREFIX/lib" \
    --with-expat="$PREFIX" \
    --with-cgal="$PREFIX"
cp config.log "${RECIPE_DIR}/../config.log"
make 2>&1 | tee "${RECIPE_DIR}/../build.log"
make check
make install

# Do not put any command that may pass between `make install` and `exit`.
exit $?
